use lazy_static::lazy_static;
use std::collections::HashMap;

// Part 1 solution
const EXAMPLE: &str = r#"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"#;

fn find_left(line: &str) -> Option<u32> {
    let c = line.chars().find(|c| c.is_numeric())?;
    c.to_digit(10)
}

fn find_right(line: &str) -> Option<u32> {
    let c = line.chars().rev().find(|c| c.is_numeric())?;
    c.to_digit(10)
}

fn value(line: &str) -> Option<u32> {
    let l = find_left(line)?;
    let r = find_right(line)?;
    Some(l * 10 + r)
}

fn sum_string(s: &str) -> u32 {
    s.lines().map(|l| value(l).unwrap()).sum()
}

// Part 2 solution
const EXAMPLE_2: &str = r#"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"#;

lazy_static! {
    static ref CANDIDATES: HashMap<&'static str, u32> = {
        let mut m = HashMap::new();
        m.insert("1", 1);
        m.insert("2", 2);
        m.insert("3", 3);
        m.insert("4", 4);
        m.insert("5", 5);
        m.insert("6", 6);
        m.insert("7", 7);
        m.insert("8", 8);
        m.insert("9", 9);
        m.insert("one", 1);
        m.insert("two", 2);
        m.insert("three", 3);
        m.insert("four", 4);
        m.insert("five", 5);
        m.insert("six", 6);
        m.insert("seven", 7);
        m.insert("eight", 8);
        m.insert("nine", 9);
        m
    };
}

fn find_left_2(line: &str) -> u32 {
    // Probably would have used regex here, but whatever
    // This is slow, because we're searching for every candidate every time
    // but it is very cute to write
    *CANDIDATES
        // iterate through candidates
        .iter()
        // Convert each entry to tuple of (index, value of candidate)
        .filter_map(|(k, v)| {
            if let Some(index) = line.find(k) {
                Some((index, v))
            } else {
                None
            }
        })
        // Sort by index to find the first found
        .min()
        // Unwrap and return the the value of earliest found
        .unwrap()
        .1
}

fn find_right_2(line: &str) -> u32 {
    // reverse the line
    let line: String = line.chars().rev().collect();
    *CANDIDATES
        // iterate through candidates
        .iter()
        // Convert each entry to tuple of (index, value of candidate)
        .filter_map(|(k, v)| {
            // reverse the key cause we are searching backwards
            let k: String = k.chars().rev().collect();
            if let Some(index) = line.find(&k) {
                Some((index, v))
            } else {
                None
            }
        })
        // Sort by index to find the first found
        .min()
        // Unwrap and return the the value of earliest found
        .unwrap()
        .1
}

fn value_2(line: &str) -> u32 {
    let l = find_left_2(line);
    let r = find_right_2(line);
    l * 10 + r
}

fn sum_string_2(s: &str) -> u32 {
    s.lines().map(|l| value_2(l)).sum()
}

#[cfg(test)]
mod test {
    use crate::day_1::{sum_string, sum_string_2, value, value_2};

    use super::{EXAMPLE, EXAMPLE_2};

    #[test]
    fn day_1_part_1_example() {
        let solution = [12 as u32, 38, 15, 77];
        let mut i = EXAMPLE.lines();
        assert_eq!(solution[0], value(i.next().unwrap()).unwrap());
        assert_eq!(solution[1], value(i.next().unwrap()).unwrap());
        assert_eq!(solution[2], value(i.next().unwrap()).unwrap());
        assert_eq!(solution[3], value(i.next().unwrap()).unwrap());
    }

    #[test]
    fn day_1_part_1() {
        let input = std::fs::read_to_string(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/assets/day_1_part_1.txt"
        ))
        .unwrap();
        let ans = sum_string(&input);
        println!("Day 1 Part 1 solution: {ans}");
    }

    #[test]
    fn day_1_part_2_example() {
        let solution = [29 as u32, 83, 13, 24, 42, 14, 76];
        let mut i = EXAMPLE_2.lines();
        assert_eq!(solution[0], value_2(i.next().unwrap()));
        assert_eq!(solution[1], value_2(i.next().unwrap()));
        assert_eq!(solution[2], value_2(i.next().unwrap()));
        assert_eq!(solution[3], value_2(i.next().unwrap()));
        assert_eq!(solution[4], value_2(i.next().unwrap()));
        assert_eq!(solution[5], value_2(i.next().unwrap()));
        assert_eq!(solution[6], value_2(i.next().unwrap()));
    }

    #[test]
    fn day_1_part_2() {
        let input = std::fs::read_to_string(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/assets/day_1_part_1.txt"
        ))
        .unwrap();
        let ans = sum_string_2(&input);
        println!("Day 1 Part 2 solution: {ans}");
    }
}
