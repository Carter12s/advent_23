use std::cmp::max;
use std::default;
use std::iter::Sum;

use nom::branch::alt;
use nom::bytes::complete::{is_a, tag};
use nom::multi::{many1, separated_list1};
use nom::number::complete::u32;
use nom::sequence::delimited;
use nom::IResult;

// Part 1:
const EXAMPLE: &str = r#"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"#;

const RED_MAX: u32 = 12;
const GREEN_MAX: u32 = 12;
const BLUE_MAX: u32 = 14;

#[derive(Default, Debug)]
pub struct Pull {
    red: u32,
    blue: u32,
    green: u32,
}

// Helper for parsing
impl Sum for Pull {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(Pull::default(), |a, b| Pull {
            red: a.red + b.red,
            blue: a.blue + b.blue,
            green: a.green + b.green,
        })
    }
}

#[derive(Debug)]
pub struct Game {
    id: u32,
    pulls: Vec<Pull>,
}

// Parses the "Game #: " header and returns the id
fn parse_header(input: &str) -> IResult<&str, u32> {
    delimited(tag("Game "), nom::character::complete::u32, tag(": "))(input)
}

// Parses a single pull "# red," returning count and color
fn parse_pull_partial(input: &str) -> IResult<&str, Pull> {
    let mut p = Pull::default();
    let (input, count) = nom::character::complete::u32(input)?;
    let input = &input[1..]; // skip 1, " "
    let (input, hit) = nom::branch::alt((tag("red"), tag("green"), tag("blue")))(input)?;
    let color = match hit {
        "red" => {
            p.red += count;
        }
        "green" => {
            p.green += count;
        }
        "blue" => {
            p.blue += count;
        }
        _ => {
            // TODO figure out how to generate nom errors?
            panic!("Sucks to be me!")
        }
    };
    Ok((input, p))
}

fn parse_pull(input: &str) -> IResult<&str, Pull> {
    let (input, pulls) = separated_list1(tag(", "), parse_pull_partial)(input)?;
    Ok((input, pulls.into_iter().sum()))
}

// Parses a full game line returning all game info
fn parse_game(input: &str) -> IResult<&str, Game> {
    let (input, id) = parse_header(input)?;
    let (input, pulls) = separated_list1(tag("; "), parse_pull)(input)?;

    Ok((input, Game { id, pulls }))
}

// Parses full problem input
fn parse_input(input: &str) -> IResult<&str, Vec<Game>> {
    separated_list1(is_a("\n\r\0"), parse_game)(input)
}

// Part 1 hardcoded limits
fn is_legal(game: &&Game) -> bool {
    game.pulls
        .iter()
        .all(|p| p.red <= 12 && p.green <= 13 && p.blue <= 14)
}

// Part 2
fn power_of_game(game: &Game) -> u32 {
    let p = game.pulls.iter().fold(Pull::default(), |a, b| Pull {
        red: max(a.red, b.red),
        blue: max(a.blue, b.blue),
        green: max(a.green, b.green),
    });
    p.red * p.blue * p.green
}

#[cfg(test)]
mod test {
    use crate::day_2::{parse_game, parse_header, parse_input, parse_pull, power_of_game};

    use super::{is_legal, EXAMPLE};

    // Messing around with using nom
    #[test]
    fn learning_nom() {
        assert_eq!(parse_header("Game 1: "), Ok(("", 1)));
        // Just make sure we parsed everything
        assert_eq!(parse_game(EXAMPLE.lines().next().unwrap()).unwrap().0, "");
        // Just make sure it parsed everything
        assert_eq!(parse_input(EXAMPLE).unwrap().0, "");
    }

    #[test]
    fn part_1_example() {
        let (_, games) = parse_input(EXAMPLE).unwrap();
        let sum: u32 = games.iter().filter(is_legal).map(|g| g.id).sum();
        assert_eq!(sum, 8);
    }

    #[test]
    fn day_2_part_1() {
        let f = std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_2.txt"))
            .unwrap();
        let (_, games) = parse_input(&f).unwrap();
        let sum: u32 = games.iter().filter(is_legal).map(|g| g.id).sum();
        println!("Day 2 Part 1 solution: {sum}");
    }

    #[test]
    fn day_2_part_2() {
        let f = std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_2.txt"))
            .unwrap();
        let (_, games) = parse_input(&f).unwrap();
        let sum: u32 = games.iter().map(power_of_game).sum();
        println!("Day 2 Part 2 solution: {sum}");
    }
}
