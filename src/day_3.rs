const EXAMPLE: &str = r#"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."#;

fn parse_input(input: &str) -> Vec<Vec<char>> {
    let mut v = vec![];
    for line in input.lines() {
        v.push(line.chars().collect())
    }
    v
}

#[derive(Clone, Debug, PartialEq)]
struct Number {
    value: u32,
    loc: (i32, i32),
    length: i32,
}

fn find_numbers(map: &Vec<Vec<char>>) -> Vec<Number> {
    let mut v = vec![];
    let mut num: Option<Number> = None;
    for (cur_y, line) in map.iter().enumerate() {
        for (cur_x, c) in line.iter().enumerate() {
            if c.is_numeric() {
                // if we're currently inside a number
                if let Some(n) = &mut num {
                    // Increment the value
                    n.value = n.value * 10 + c.to_digit(10).unwrap();
                    n.length += 1;
                } else {
                    // Start a new number
                    num = Some(Number {
                        value: c.to_digit(10).unwrap(),
                        loc: (cur_x as i32, cur_y as i32),
                        length: 1,
                    });
                }
            } else {
                // We've reached the end of a number
                if let Some(n) = &num {
                    // Push it
                    v.push(n.clone());
                    num = None;
                }
            }
        }
        if let Some(n) = &num {
            // We've reached the end of a line mid-number
            v.push(n.clone());
            num = None;
        }
    }
    v
}

fn find_symbols(map: &Vec<Vec<char>>) -> Vec<(i32, i32, char)> {
    let mut syms = vec![];
    for (y, line) in map.iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if !c.is_numeric() && *c != '.' {
                syms.push((x as i32, y as i32, c.clone()))
            }
        }
    }
    syms
}

fn check(loc: &(i32, i32), syms: &Vec<(i32, i32, char)>) -> bool {
    syms.iter().find(|i| i.0 == loc.0 && i.1 == loc.1).is_some()
}

fn is_valid(num: &Number, syms: &Vec<(i32, i32, char)>) -> bool {
    let mut valid = false;
    // check left
    valid |= check(&(num.loc.0 - 1, num.loc.1), syms);
    // from left to right check above and below
    for dx in -1..=num.length {
        valid |= check(&(num.loc.0 + dx, num.loc.1 + 1), syms);
        valid |= check(&(num.loc.0 + dx, num.loc.1 - 1), syms);
    }
    // check right
    valid |= check(&(num.loc.0 + num.length, num.loc.1), syms);
    valid
}

fn sum_valid(nums: &Vec<Number>, syms: &Vec<(i32, i32, char)>) -> u32 {
    let is_valid = |x: &&Number| is_valid(x, syms);
    nums.iter().filter(is_valid).map(|n| n.value).sum()
}

fn get_adj_nums<'a>(sym: &(i32, i32, char), nums: &'a Vec<Number>) -> Vec<&'a Number> {
    let mut vec = vec![];
    for num in nums {
        if is_valid(num, &vec![*sym]) {
            vec.push(num);
        }
    }
    vec
}

#[cfg(test)]
mod test {
    use crate::day_3::{find_symbols, is_valid, Number};

    use super::{find_numbers, get_adj_nums, parse_input, sum_valid, EXAMPLE};

    #[test]
    fn is_valid_test() {
        const INPUT: &str = r#".....
.123.
....."#;
        let map = parse_input(INPUT);
        let nums = find_numbers(&map);
        assert_eq!(
            nums,
            vec![Number {
                value: 123,
                loc: (1, 1),
                length: 3
            }]
        );
        let n = nums.get(0).unwrap();

        assert!(is_valid(n, &vec![(0, 0, '*')]));
        assert!(is_valid(n, &vec![(1, 0, '*')]));
        assert!(is_valid(n, &vec![(2, 0, '*')]));
        assert!(is_valid(n, &vec![(3, 0, '*')]));
        assert!(is_valid(n, &vec![(4, 0, '*')]));

        assert!(is_valid(n, &vec![(0, 1, '*')]));
        assert!(is_valid(n, &vec![(4, 1, '*')]));

        assert!(is_valid(n, &vec![(0, 2, '*')]));
        assert!(is_valid(n, &vec![(1, 2, '*')]));
        assert!(is_valid(n, &vec![(2, 2, '*')]));
        assert!(is_valid(n, &vec![(3, 2, '*')]));
        assert!(is_valid(n, &vec![(4, 2, '*')]));

        assert!(!is_valid(n, &vec![(5, 0, '*')]));
        assert!(!is_valid(n, &vec![(5, 1, '*')]));
        assert!(!is_valid(n, &vec![(5, 2, '*')]));
        assert!(!is_valid(n, &vec![(-1, 0, '*')]));
        assert!(!is_valid(n, &vec![(-1, 1, '*')]));
        assert!(!is_valid(n, &vec![(-1, 2, '*')]));

        let syms = find_symbols(&map);
        assert!(syms.is_empty());
    }

    #[test]
    fn part_1_example() {
        let map = parse_input(EXAMPLE);
        println!("{map:?}");
        let nums = find_numbers(&map);

        let syms = find_symbols(&map);
        println!("{syms:?}");
        let sum = sum_valid(&nums, &syms);
        assert_eq!(sum, 4361);
    }

    #[test]
    fn part_1() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_3.txt"))
                .unwrap();
        let map = parse_input(&input);
        let nums = find_numbers(&map);
        let syms = find_symbols(&map);
        let sum = sum_valid(&nums, &syms);
        println!("Day 3 part 1 solution is: {sum}");
    }

    #[test]
    fn part_2() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_3.txt"))
                .unwrap();
        let map = parse_input(&input);
        let nums = find_numbers(&map);
        let syms = find_symbols(&map);
        let stars: Vec<_> = syms.iter().filter(|i| i.2 == '*').collect();

        let sum: u32 = stars
            .iter()
            .map(|s| get_adj_nums(s, &nums))
            .filter_map(|v| {
                if v.len() != 2 {
                    None
                } else {
                    Some(v.get(0).unwrap().value * v.get(1).unwrap().value)
                }
            })
            .sum();

        println!("Day 3 part 2 solution is : {sum}");
    }
}
