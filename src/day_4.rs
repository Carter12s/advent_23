use nom::{
    branch::alt,
    bytes::complete::{is_a, tag},
    character::complete::{multispace0, multispace1, u32},
    combinator::{all_consuming, eof},
    multi::{separated_list0, separated_list1},
    sequence::{delimited, preceded, terminated, tuple},
    Finish, IResult,
};

const EXAMPLE: &str = r#"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"#;

#[derive(Debug)]
struct Card {
    id: u32,
    winners: Vec<u32>,
    nums: Vec<u32>,
}

fn nom_card(input: &str) -> IResult<&str, Card> {
    let (input, id) = delimited(tuple((tag("Card"), multispace1)), u32, tag(":"))(input)?;
    let (input, winners) = preceded(multispace1, separated_list1(multispace1, u32))(input)?;
    let (input, nums) = preceded(
        tuple((tag(" |"), multispace0)),
        separated_list1(multispace1, u32),
    )(input)?;
    Ok((input, Card { id, winners, nums }))
}

fn nom_input(input: &str) -> Vec<Card> {
    input.lines().map(|l| nom_card(l).unwrap().1).collect()
}

fn score_card(card: &Card) -> usize {
    let matches = card
        .nums
        .iter()
        .filter(|num| card.winners.contains(num))
        .count();
    if matches > 0 {
        (2 as usize).pow(matches as u32 - 1)
    } else {
        0
    }
}

fn count_cards(cards: &Vec<Card>) -> u32 {
    let count_match = |c: &Card| c.nums.iter().filter(|num| c.winners.contains(num)).count();
    let matches: Vec<_> = cards.iter().map(count_match).collect();
    let mut unprocessed = vec![1; matches.len()];
    let mut count = 0;
    for i in 0..matches.len() {
        count += unprocessed[i];
        let score = matches[i];
        for j in 1..=score {
            unprocessed[i + j] += unprocessed[i];
        }
    }
    count
}

#[cfg(test)]
mod test {
    use std::collections::VecDeque;

    use crate::day_4::{count_cards, score_card};

    use super::{nom_input, EXAMPLE};

    #[test]
    fn part_1_example() {
        let cards = nom_input(EXAMPLE);
        println!("cards: {cards:?}");
        assert_eq!(cards.len(), 6);

        let score: usize = cards.iter().map(score_card).sum();
        assert_eq!(score, 13);
    }

    #[test]
    fn part_1() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_4.txt"))
                .unwrap();
        let cards = nom_input(&input);
        assert_eq!(cards.len(), 208);
        let score: usize = cards.iter().map(score_card).sum();
        println!("Day 4 part 1 = {score}");
    }

    #[test]
    fn part_2_example() {
        let cards = nom_input(EXAMPLE);
        assert_eq!(cards.len(), 6);
        let count = count_cards(&cards);
        assert_eq!(count, 30);
    }

    #[test]
    fn part_2() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_4.txt"))
                .unwrap();
        let cards = nom_input(&input);
        assert_eq!(cards.len(), 208);
        let count = count_cards(&cards);
        println!("Day 4 part 2 = {count}");
    }
}
