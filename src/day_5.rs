use rayon::prelude::*;
use nom::{
    bytes::complete::{is_a, tag, take_till1, take_while1},
    character::{
        complete::{alpha1, multispace0, multispace1},
        is_alphabetic,
    },
    multi::{many0, many1, separated_list1},
    sequence::{delimited, preceded, tuple},
    IResult,
};

const EXAMPLE: &str = r#"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"#;

struct RawInput<'a> {
    seeds: Vec<u64>,
    maps: Vec<(&'a str, Vec<Range>)>,
}

#[derive(PartialEq, Debug)]
struct Range {
    dst: u64,
    src: u64,
    len: u64,
}

fn parse_seeds(input: &str) -> IResult<&str, Vec<u64>> {
    preceded(
        tag("seeds: "),
        separated_list1(multispace1, nom::character::complete::u64),
    )(input)
}

fn parse_range(input: &str) -> IResult<&str, Range> {
    fn parse_num(input: &str) -> IResult<&str, u64> {
        delimited(
            many0(is_a(" ")),
            nom::character::complete::u64,
            many0(is_a(" ")),
        )(input)
    }
    let (input, (dst, src, len)) = tuple((parse_num, parse_num, parse_num))(input)?;
    Ok((input, Range { dst, src, len }))
}

fn parse_map(input: &str) -> IResult<&str, (&str, Vec<Range>)> {
    let (input, name) = delimited(multispace0, take_till1(|c| c == ' '), is_a("map: \r\n"))(input)?;
    let (input, nums) = separated_list1(is_a("\r\n"), parse_range)(input)?;
    Ok((input, (name, nums)))
}

fn parse_input(input: &str) -> RawInput {
    let (input, seeds) = parse_seeds(input).unwrap();
    let (input, maps) = many1(parse_map)(input).unwrap();
    RawInput { seeds, maps }
}

impl Range {
    fn map(&self, num: u64) -> Option<u64> {
        if (self.src..(self.src + self.len)).contains(&num) {
            Some(self.dst + num - self.src)
        } else {
            None
        }
    }
}

fn map(num: u64, map: &Vec<Range>) -> u64 {
    for r in map {
        if let Some(out) = r.map(num) {
            return out;
        }
    }
    // Default case is fall through
    num
}

fn map_seed(seed: u64, input: &RawInput) -> u64 {
    let x = input.maps.iter().fold(seed, |prev, (_, r)| map(prev, r));
    x
}

fn lowest_loc(input: &RawInput) -> u64 {
    input
        .seeds
        .iter()
        .map(|s| map_seed(*s, input))
        .min()
        .unwrap()
}

// Same as part 1, but understand ranges
fn lowest_loc_range(input: &RawInput) -> u64 {
    input
        .seeds
        .par_iter()
        .chunks(2)
        .map(|s| (*s[0]..(s[0] + s[1])).into_par_iter().map(|s| map_seed(s, input)).min())
        .min()
        .unwrap()
        .unwrap()
}

#[cfg(test)]
mod test {
    use crate::day_5::{parse_input, parse_range, Range, RawInput, EXAMPLE, lowest_loc_range};

    use super::{lowest_loc, parse_map, parse_seeds};

    #[test]
    fn parsing() {
        let (i, p) = parse_seeds("seeds: 1 2 3\n\nf").unwrap();
        assert_eq!(i, "\n\nf");
        assert_eq!(p, vec![1, 2, 3]);

        let (i, p) = parse_range("1 2 3\n4 5 6\n\n").unwrap();
        assert_eq!(i, "\n4 5 6\n\n");

        let (i, (n, p)) = parse_map("\n\nme-to-you map:\n1 2 3\n4 5 6\n\nother").unwrap();
        assert_eq!(i, "\n\nother");
        assert_eq!(n, "me-to-you");
        assert_eq!(
            p,
            vec![
                Range {
                    dst: 1,
                    src: 2,
                    len: 3
                },
                Range {
                    dst: 4,
                    src: 5,
                    len: 6
                }
            ]
        );

        let input = parse_input(EXAMPLE);
        assert_eq!(input.seeds.len(), 4);
        assert_eq!(input.maps.len(), 7);
        assert_eq!(input.maps[6].1.len(), 2);
    }

    #[test]
    fn part_1_example() {
        let input = parse_input(EXAMPLE);
        let res = lowest_loc(&input);
        assert_eq!(res, 35);
        let res2 = lowest_loc_range(&input);
        assert_eq!(res2, 46);
    }

    #[test]
    fn part_1() {
        let p = concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_5.txt");
        let input = std::fs::read_to_string(p).unwrap();
        let input = parse_input(&input);
        let res = lowest_loc(&input);
        println!("Day 5 part 1: {res}");
        // let res2 = lowest_loc_range(&input);
        // println!("Day 5 part 2: {res2}");
    }
}
