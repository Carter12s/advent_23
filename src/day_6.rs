use nom::{
    bytes::complete::tag,
    character::complete::multispace1,
    sequence::{pair, preceded},
    IResult,
};

const EXAMPLE: &str = "Time:      7  15   30\n
Distance:  9  40  200";

const INPUT: &str = "Time:        59     68     82     74\n
Distance:   543   1020   1664   1022";

const PART_2_EXAMPLE_TIME: u64 = 71530;
const PART_2_EXAMPLE_DIST: u64 = 940200;

const PART_2_TIME: u64 = 59_688_274;
const PART_2_DISTANCE: u64 = 543_102_016_641_022;

// Parses the Time and Distance fields from the Example
fn parse_input(input: &str) -> IResult<&str, (Vec<u64>, Vec<u64>)> {
    let (input, times) = preceded(
        pair(tag("Time:"), multispace1),
        nom::multi::separated_list1(
            nom::character::complete::space1,
            nom::character::complete::u64,
        ),
    )(input)?;
    let (input, _) = multispace1(input)?;
    let (input, distances) = preceded(
        pair(tag("Distance:"), multispace1),
        nom::multi::separated_list1(
            nom::character::complete::space1,
            nom::character::complete::u64,
        ),
    )(input)?;
    Ok((input, (times, distances)))
}

fn num_ways_to_win(time: &u64, distance: &u64) -> u64 {
    // Solution to polynomial from wolfram alpha
    let time = *time as f64;
    let distance = (distance + 1) as f64;
    let inner = (time * time - 4.0 * distance) as f64;
    let inner_root = inner.sqrt();
    let t_min_raw = ((time - inner_root) / 2.0);
    let t_max_raw = ((time + inner_root) / 2.0);
    let t_min = t_min_raw.ceil() as u64;
    let t_max = t_max_raw.floor() as u64;
    let tot = t_max - t_min + 1;
    println!("Case {time}.{distance}= tot={tot}, t_max={t_max}@{t_max_raw} , t_min={t_min}@{t_min_raw}, inner={inner}, inner_root={inner_root}");
    tot as u64
}

fn find_num_ways_to_win(times: &Vec<u64>, distances: &Vec<u64>) -> Vec<u64> {
    let mut ways_to_win = vec![];
    for (time, distance) in times.iter().zip(distances.iter()) {
        ways_to_win.push(num_ways_to_win(time, distance))
    }
    ways_to_win
}

#[cfg(test)]
mod test {
    use crate::day_6::num_ways_to_win;


    #[test]
    fn parse_input() {
        let input = super::EXAMPLE;
        let (input, (times, distances)) = super::parse_input(input).unwrap();
        assert_eq!(input, "");
        assert_eq!(times, vec![7, 15, 30]);
        assert_eq!(distances, vec![9, 40, 200]);
    }

    // Completes the example from the advent of code day 6
    #[test]
    fn example() {
        let t1 = num_ways_to_win(&7, &9);
        assert_eq!(t1, 4);


        let (times, distances) = super::parse_input(super::EXAMPLE).unwrap().1;
        let ways_to_win = super::find_num_ways_to_win(&times, &distances);
        let answer = ways_to_win.iter().fold(1, |acc, x| acc * x);
        assert_eq!(answer, 288);
    }
    
    #[test]
    fn part_1() {
        let (times, distances) = super::parse_input(super::INPUT).unwrap().1;
        let ways_to_win = super::find_num_ways_to_win(&times, &distances);
        let answer = ways_to_win.iter().fold(1, |acc, x| acc * x);
        println!("Part 1: {}", answer);
    }

    #[test]
    fn part_2_example() {
        let answer = num_ways_to_win(&super::PART_2_EXAMPLE_TIME, &super::PART_2_EXAMPLE_DIST);
        assert_eq!(answer, 71503);
    }

    #[test]
    fn part_2_cool() {
        let answer = num_ways_to_win(&super::PART_2_TIME, &super::PART_2_DISTANCE);
        println!("Part 2: {}", answer);
    }

}
