use std::collections::HashMap;

use nom::{character::complete::multispace1, sequence::delimited, IResult};

const EXAMPLE: &str = "32T3K 765\n
T55J5 684\n
KK677 28\n
KTJJT 220\n
QQQJA 483\n";

#[derive(Debug, PartialEq)]
struct Hand<'a>(&'a str);

fn parse_hand(input: &str) -> IResult<&str, Hand> {
    nom::bytes::complete::take(5usize)(input).map(|(input, hand)| (input, Hand(hand)))
}

// Parse the input into a Vec of (String, u64)
fn parse_input(input: &str) -> IResult<&str, Vec<(Hand, u64)>> {
    nom::multi::many1(nom::sequence::pair(
        parse_hand,
        delimited(multispace1, nom::character::complete::u64, multispace1),
    ))(input)
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Type {
    FiveOfAKind = 7,
    FourOfAKind = 6,
    FullHouse = 5,
    ThreeOfAKind = 4,
    TwoPairs = 3,
    OnePair = 2,
    HighCard = 1,
}

fn get_type(h: &str) -> Type {
    let mut counts = HashMap::new();
    for c in h.chars() {
        let count = counts.entry(c).or_insert(0);
        *count += 1;
    }
    match counts.len() {
        1 => Type::FiveOfAKind,
        2 => {
            if counts.values().any(|&v| v == 4) {
                Type::FourOfAKind
            } else {
                Type::FullHouse
            }
        }
        3 => {
            if counts.values().any(|&v| v == 3) {
                Type::ThreeOfAKind
            } else {
                Type::TwoPairs
            }
        }
        4 => Type::OnePair,
        5 => Type::HighCard,
        _ => panic!("Invalid hand"),
    }
}

fn get_type_joker(h: &str) -> Type {
    let mut counts = HashMap::new();
    let mut jokers = 0;
    for c in h.chars() {
        if c == 'J' {
            jokers += 1;
            continue;
        }
        let count = counts.entry(c).or_insert(0);
        *count += 1;
    }
    let Some(max_count) = counts.values().max() else {
        // 5 Jokers
        return Type::FiveOfAKind;
    };
    // println!("{h}: {counts:?} {max_count} {jokers}");
    // Is 5 of kind
    if counts.len() == 1 || counts.len() == 0 {
        return Type::FiveOfAKind;
    }
    // Is 4 of kind
    if max_count + jokers == 4 {
        return Type::FourOfAKind;
    }
    // Is FullHouse
    if *max_count == 3 && counts.values().any(|&v| v == 2) {
        return Type::FullHouse;
    }
    if counts.len() == 2 {
        let values = counts.values().collect::<Vec<_>>();
        if values == vec![&2, &2] && jokers == 1 {
            return Type::FullHouse;
        }
    }
    // Is 3 of kind
    if max_count + jokers == 3 {
        return Type::ThreeOfAKind;
    }
    // Is 2 pairs
    if counts.len() == 3 && counts.values().any(|&v| v == 2) {
        return Type::TwoPairs;
    }
    // Is 1 pair
    if max_count + jokers == 2 {
        return Type::OnePair;
    }
    // Is high card
    if *max_count == 1 && jokers == 0 {
        return Type::HighCard;
    }
    panic!("Invalid hand");
}

impl PartialOrd for Type {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let self_val = *self as u8;
        let other_val = *other as u8;
        self_val.partial_cmp(&other_val)
    }
}

impl<'a> PartialOrd for Hand<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let self_type = get_type(self.0);
        let other_type = get_type(other.0);
        let type_comparison = self_type.partial_cmp(&other_type);
        match type_comparison {
            Some(std::cmp::Ordering::Equal) => {
                for (a, b) in { self.0.chars().zip(other.0.chars()) } {
                    let a_val = val_from_char(a);
                    let b_val = val_from_char(b);
                    let cmp = a_val.partial_cmp(&b_val);
                    if let Some(std::cmp::Ordering::Equal) = cmp {
                        continue;
                    } else {
                        return cmp;
                    }
                }
                return Some(std::cmp::Ordering::Equal);
            }
            _ => type_comparison,
        }
    }
}

fn joker_cmp(a: &Hand, b: &Hand) -> std::cmp::Ordering {
    let a_type = get_type_joker(a.0);
    let b_type = get_type_joker(b.0);
    let type_comparison = a_type.partial_cmp(&b_type);
    match type_comparison {
        Some(std::cmp::Ordering::Equal) => {
            for (a, b) in { a.0.chars().zip(b.0.chars()) } {
                let a_val = val_from_char_joker(a);
                let b_val = val_from_char_joker(b);
                let cmp = a_val.partial_cmp(&b_val);
                if let Some(std::cmp::Ordering::Equal) = cmp {
                    continue;
                } else {
                    return cmp.unwrap();
                }
            }
            return std::cmp::Ordering::Equal;
        }
        _ => type_comparison.unwrap(),
    }
}

fn val_from_char(c: char) -> u64 {
    match c {
        'A' => 14,
        'K' => 13,
        'Q' => 12,
        'J' => 11,
        'T' => 10,
        _ => c.to_digit(10).unwrap() as u64,
    }
}

fn val_from_char_joker(c: char) -> u64 {
    match c {
        'A' => 14,
        'K' => 13,
        'Q' => 12,
        'J' => 0,
        'T' => 10,
        _ => c.to_digit(10).unwrap() as u64,
    }
}

fn score_hands<'a>(hands: &mut Vec<(Hand<'a>, u64)>) -> u64 {
    hands.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
    let mut score = 0;
    for (i, (hand, value)) in hands.iter().enumerate() {
        score += (i + 1) as u64 * value;
        let cur_type = get_type(hand.0);
        println!("{hand:?} {i} {value} {cur_type:?}");
    }
    score
}

fn score_hands_joker<'a>(hands: &mut Vec<(Hand<'a>, u64)>) -> u64 {
    hands.sort_by(|a, b| joker_cmp(&a.0, &b.0));
    let mut score = 0;
    for (i, (hand, value)) in hands.iter().enumerate() {
        score += (i + 1) as u64 * value;
        let cur_type = get_type_joker(hand.0);
        println!("{hand:?} {i} {value} {cur_type:?}");
    }
    score
}

#[cfg(test)]
mod test {
    use crate::day_7::Hand;

    #[test]
    fn test_parse_input() {
        let input = super::EXAMPLE;
        let (input, parsed) = super::parse_input(input).unwrap();
        assert_eq!(input, "");
        assert_eq!(
            parsed,
            vec![
                (Hand("32T3K"), 765),
                (Hand("T55J5"), 684),
                (Hand("KK677"), 28),
                (Hand("KTJJT"), 220),
                (Hand("QQQJA"), 483)
            ]
        );
    }

    #[test]
    fn test_get_type() {
        assert_eq!(super::get_type("32T4K"), super::Type::HighCard);
        assert_eq!(super::get_type("T53J5"), super::Type::OnePair);
        assert_eq!(super::get_type("KK677"), super::Type::TwoPairs);
        assert_eq!(super::get_type("KTJJJ"), super::Type::ThreeOfAKind);
        assert_eq!(super::get_type("QQQTT"), super::Type::FullHouse);
    }

    #[test]
    fn test_compare_hand() {
        assert!(Hand("32T4K") < Hand("T53J5"));
        assert!(Hand("T53J5") < Hand("KK677"));
        assert!(Hand("KK677") < Hand("KTJJJ"));
        assert!(Hand("KTJJJ") < Hand("QQQTT"));
        assert!(Hand("TQQQT") < Hand("AQQQA"));
        assert!(Hand("32T4K") < Hand("T53J5"));
        assert!(Hand("KK677") > Hand("KTJJT"));
    }

    #[test]
    fn test_score_example() {
        let input = super::EXAMPLE;
        let (_, mut parsed) = super::parse_input(input).unwrap();
        let score = super::score_hands(&mut parsed);
        assert_eq!(score, 6440);
    }

    #[test]
    fn part_1() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_7.txt"))
                .unwrap();
        let (_, mut parsed) = super::parse_input(&input).unwrap();
        let score = super::score_hands(&mut parsed);
        println!("Part 1: {}", score);
    }

    #[test]
    fn get_type_joker() {
        assert_eq!(super::get_type_joker("32T4K"), super::Type::HighCard);
        assert_eq!(super::get_type_joker("T53J5"), super::Type::ThreeOfAKind);
        assert_eq!(super::get_type_joker("KK677"), super::Type::TwoPairs);
        assert_eq!(super::get_type_joker("KTJJJ"), super::Type::FourOfAKind);
        assert_eq!(super::get_type_joker("QQQTT"), super::Type::FullHouse);
        assert_eq!(super::get_type_joker("KJJJJ"), super::Type::FiveOfAKind);
    }

    #[test]
    fn example_part_2() {
        let input = super::EXAMPLE;
        let (_, mut parsed) = super::parse_input(input).unwrap();
        let score = super::score_hands_joker(&mut parsed);
        assert_eq!(score, 5905);
    }

    #[test]
    fn part_2() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_7.txt"))
                .unwrap();
        let (_, mut parsed) = super::parse_input(&input).unwrap();
        let score = super::score_hands_joker(&mut parsed);
        println!("Part 2: {}", score);
    }
}
