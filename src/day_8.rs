use std::{collections::HashMap, hash::Hash};

use nom::{
    bytes::complete::{is_a, tag},
    character::{is_alphabetic, is_newline},
    IResult,
};

const EXAMPLE: &str = r#"RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)"#;

const EXAMPLE_2: &str = r#"LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)"#;

const EXAMPLE_3: &str = r#"LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)"#;

// Parses a line in format 'AAA = (BBB, CCC)'
// And returns ('AAA', ('BBB', 'CCC'))
fn parse_line(input: &str) -> IResult<&str, (&str, (&str, &str))> {
    let (input, key) = nom::bytes::complete::take(3usize)(input)?;
    let (input, _) = nom::bytes::complete::tag(" = (")(input)?;
    let (input, left) = nom::bytes::complete::take(3usize)(input)?;
    let (input, _) = nom::bytes::complete::tag(", ")(input)?;
    let (input, right) = nom::bytes::complete::take(3usize)(input)?;
    let (input, _) = nom::bytes::complete::tag(")")(input)?;
    let (input, _) = nom::character::complete::multispace0(input)?;
    Ok((input, (key, (left, right))))
}

fn parse_input(input: &str) -> IResult<&str, (&str, HashMap<&str, (&str, &str)>)> {
    let (input, code) = nom::character::complete::alpha1(input)?;
    let (input, _) = nom::character::complete::multispace1(input)?;

    let mut map = HashMap::new();
    for line in input.lines() {
        let (input, (key, (left, right))) = parse_line(line).unwrap();
        map.insert(key, (left, right));
    }
    Ok(("", (code, map)))
}

// Walking hashmap tree manually without recursion
// Returns the number of steps taken to walk from "AAA" to "ZZZ"
fn walk_tree(path: &str, tree: &HashMap<&str, (&str, &str)>) -> u64 {
    let mut iter = path.chars().cycle();
    let mut tot = 0;
    let mut cur_key = "AAA";
    loop {
        let c = iter.next().unwrap();
        let (left, right) = tree.get(cur_key).unwrap();
        if c == 'L' {
            cur_key = left;
        } else {
            cur_key = right;
        }
        tot += 1;
        if cur_key == "ZZZ" {
            break;
        }
    }
    tot
}

// Simultaneous walking from all nodes that end with "A" to all nodes that end with "Z"
// Without recursion
// Report the minimum number of steps taken to walk until all nodes end with "Z"
fn simul_walk_tree(path: &str, tree: &HashMap<&str, (&str, &str)>) -> u64 {
    let mut iter = path.chars().cycle();
    let mut tot = 0;
    let mut cur_keys = tree
        .keys()
        .filter(|k| k.ends_with('A'))
        .map(|k| *k)
        .collect::<Vec<_>>();
    let mut next_keys = Vec::new();
    loop {
        let c = iter.next().unwrap();
        next_keys.clear();
        for cur_key in &cur_keys {
            let (left, right) = tree.get(cur_key).unwrap();
            if c == 'L' {
                next_keys.push(*left);
            } else {
                next_keys.push(*right);
            }
        }
        tot += 1;
        if next_keys.iter().all(|k| k.ends_with('Z')) {
            break;
        }
        if tot % 1_000_000 == 0 {
            println!("{tot} {next_keys:?}");
        }
        // Swap
        (cur_keys, next_keys) = (next_keys, cur_keys);
    }
    tot
}

// Walks the tree from the given starting node
// Until we arrive back at the same node on the same instruction
// At this point we've found a loop and report the intervals at which
// this node will yield nodes that end in Z
fn walk_until_loop<'a>(
    path: &'a str,
    tree: &HashMap<&'a str, (&'a str, &'a str)>,
    node: &'a str,
) -> (usize, usize) {
    let mut iter = path.chars().enumerate().cycle();
    let mut visited = vec![];
    let mut cur_key = node;
    let mut tot = 0;
    let mut i = 0;
    let mut c = ' ';
    loop {
        (i, c) = iter.next().unwrap();
        let (left, right) = tree.get(cur_key).unwrap();
        tot += 1;
        match c {
            'L' => cur_key = left,
            'R' => cur_key = right,
            _ => panic!("Invalid instruction"),
        }
        if visited.contains(&(i, cur_key)) {
            break;
        }
        visited.push((i, cur_key));
    }
    // Now that we've found a loop we want to return
    // The "phase" of the loop and the periods of the loop
    // That land on a node ending in Z
    // Note: we're cheating, because after checking input each "seed" only has one period
    let loop_start = visited
        .iter()
        .position(|(j, k)| *j == i && *k == cur_key)
        .unwrap();
    let period = tot - loop_start;
    // How many steps until we reached Z for the first time
    let phase = visited.iter().position(|(j, k)| k.ends_with("Z")).unwrap();
    (phase, period)
}

fn get_all_phases_and_periods<'a>(
    path: &str,
    tree: &'a HashMap<&str, (&str, &str)>,
) -> HashMap<&'a str, (usize, usize)> {
    let mut map = HashMap::new();
    for node in tree.keys().filter(|k| k.ends_with("A")) {
        let (phase, periods) = walk_until_loop(path, tree, node);
        map.insert(*node, (phase, periods));
    }
    map
}

// From the phases and periods of each node figure out when they will align
// Based off of this: https://math.stackexchange.com/questions/2218763/how-to-find-lcm-of-two-numbers-when-one-starts-with-an-offset
fn calculate_beat(beat_map: &HashMap<&str, (usize, usize)>) -> usize {
    let mut beat = 0;
    // Each seed starts on the phase
    let step = beat_map
        .values()
        .map(|(_phase, periods)| periods)
        .min()
        .unwrap();
    loop {
        beat += step;
        if beat_map
            .values()
            .all(|(phase, period)| (beat + phase) % period == 0)
        {
            break;
        }
        println!("{beat}");
    }
    beat
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_line() {
        assert_eq!(
            parse_line("AAA = (BBB, CCC)\r\n"),
            Ok(("", ("AAA", ("BBB", "CCC"))))
        );
    }

    #[test]
    fn test_parse_input() {
        assert_eq!(
            parse_input(EXAMPLE),
            Ok((
                "",
                (
                    "RL",
                    vec![
                        ("AAA", ("BBB", "CCC")),
                        ("BBB", ("DDD", "EEE")),
                        ("CCC", ("ZZZ", "GGG")),
                        ("DDD", ("DDD", "DDD")),
                        ("EEE", ("EEE", "EEE")),
                        ("GGG", ("GGG", "GGG")),
                        ("ZZZ", ("ZZZ", "ZZZ")),
                    ]
                    .into_iter()
                    .collect()
                )
            ))
        );
    }

    #[test]
    fn walk_tree_test() {
        let (_, (path, map)) = parse_input(EXAMPLE).unwrap();
        assert_eq!(walk_tree(path, &map), 2);

        let (_, (path, map)) = parse_input(EXAMPLE_2).unwrap();
        assert_eq!(walk_tree(path, &map), 6);
    }

    #[test]
    fn part_1() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_8.txt"))
                .unwrap();
        let (_, (path, map)) = parse_input(&input).unwrap();
        let tot = walk_tree(path, &map);
        println!("Part 1: {}", tot)
    }

    #[test]
    fn simul_walk_tree_test() {
        let (_, (path, map)) = parse_input(EXAMPLE_3).unwrap();
        assert_eq!(simul_walk_tree(path, &map), 6);
    }

    // This solution to part 2 ran forever
    // #[test]
    // fn part_2() {
    //     let input =
    //         std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_8.txt"))
    //             .unwrap();
    //     let (_, (path, map)) = parse_input(&input).unwrap();
    //     let tot = simul_walk_tree(path, &map);
    //     println!("Part 2: {}", tot)
    // }

    #[test]
    fn walk_until_loop_test() {
        let input =
            std::fs::read_to_string(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/day_8.txt"))
                .unwrap();
        let (_, (path, map)) = parse_input(&input).unwrap();
        let map = get_all_phases_and_periods(path, &map);
        println!("Map for input: {map:?}");
    }

    #[test]
    fn walk_until_example() {
        let (_, (path, tree)) = parse_input(EXAMPLE_3).unwrap();
        let map = get_all_phases_and_periods(path, &tree);
        println!("Phase and period map: {map:?}");
        // let beat = calculate_beat(&map);
        // assert_eq!(beat, 6);
    }
}
